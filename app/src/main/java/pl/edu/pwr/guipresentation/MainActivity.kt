package pl.edu.pwr.guipresentation

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import pl.edu.pwr.guipresentation.ui.theme.GUIPresentationTheme

class MainActivity : ComponentActivity() {

    private val money: MutableState<Int> = mutableStateOf(0)
    private var doubleItText = "Double it!"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            GUIPresentationTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Greeting(money = money,
                        addMoney = {
                        money.value += 1
                        doubleItText = "Triple it!"
                        Log.e("Wartosc double it", doubleItText)
                    },
                        doubleItText = doubleItText
                    )
                }
            }
        }
    }
}

@Composable
fun Greeting(money: MutableState<Int>, addMoney: () -> Unit, doubleItText: String) {
    val counter = remember {
        mutableStateOf(0)
    }
    val inputValue = remember {
        mutableStateOf("")
    }

    //TODO istnieje jeszcze ConstraintLayout oraz Box

    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.padding(10.dp)
    ) {
        Text(
            text = "This is casual text",
        )

        // na tlumaczenie
        Text(text = stringResource(id = R.string.app_name), color = Color.Magenta)


        Row(
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            // wywolanie przekazanej funkcji i wyswietlenie przekazanego parametru
            IconButton(onClick = { addMoney() }) {
                Icon(
                    painter = painterResource(id = R.drawable.baseline_attach_money_24),
                    contentDescription = "money"
                )
            }
            Text(text = money.value.toString())
        }

        // wyswietlenie textu, ktory jest parametrem typu String
        Text(text = doubleItText)


        // funkcja zadeklarowana w przycisku i modyfikacja wartosci lokalnie zadeklarowanej
        Button(onClick = { counter.value += 1 }) {
            Text(text = stringResource(id = R.string.click_me))
        }

        Text(text = counter.value.toString())


        // pole na dane wejsciowe i przyklad jak inne pole korzysta
        TextField(value = inputValue.value, onValueChange = { newValue ->
            inputValue.value = newValue
            Log.e("aaa", newValue)
        })

        Text(text = inputValue.value)
    }

}

@SuppressLint("UnrememberedMutableState")
@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    val money: MutableState<Int> = mutableStateOf(0)

    GUIPresentationTheme {
        Greeting(money, {}, "test")
    }
}